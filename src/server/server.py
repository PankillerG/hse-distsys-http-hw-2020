import os

from flask import Flask, request

import json
import logging

data = dict()
app = Flask(__name__)

# app.logger.info('!')
@app.route('/data', methods=['GET', 'POST'])
def _data():
    global data
    if request.method == 'GET':
        return json.dumps(data)

    elif request.method == 'POST':
        key = request.form['key']
        value = request.form['value']
        if key in data.keys():
            return json.dumps({})
        data[key] = value
        return json.dumps(request.form)

@app.route('/data/<string:key>', methods=['GET', 'PUT', 'DELETE'])
def _data_key(key):
    global data
    if request.method == 'GET':
        if key not in data.keys():
            return json.dumps({})
        return json.dumps(data[key])

    elif request.method == 'PUT':
        value = request.form['value']
        if key not in data.keys():
            return json.dumps({})
        data[key] = value
        return json.dumps(request.form)

    elif request.method == 'DELETE':
        if key not in data.keys():
            return json.dumps({})
        data.pop(key, None)
        return json.dumps('deleted')

app.run(host='0.0.0.0', port=int(os.environ.get('HSE_HTTP_FLASK_PORT', 80)), debug=True)