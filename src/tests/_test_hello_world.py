import os

import requests

import json
from collections import defaultdict 

SERVER_HOST = os.environ.get('HSE_HTTP_TESTS_SERVER_HOST', 'server')
SERVER_PORT = int(os.environ.get('HSE_HTTP_FLASK_PORT', 80))
URL = 'http://' + SERVER_HOST
if SERVER_PORT != 80:
    URL += ':{}'.format(SERVER_PORT)

data = defaultdict(lambda: None)

# def test_hello_world():
#     response = requests.get(URL + '/')
#     assert response.text == 'Hello, World!'
#     assert response.status_code == 200

# def test_get():
#     response = requests.get(URL + '/data')
#     print(json.loads(response.text))
#     assert response.text == '{}'
#     assert response.status_code == 200

def st_post(key, value):
    tmp_data = {'key':key, 'value':value}
    response = requests.post(URL + '/data', data=tmp_data)
    if data[key] is None:
        data[key] = value
    else:
        tmp_data = {}
    assert response.text == json.dumps(tmp_data)
    assert response.status_code == 200

def st_get_id(key):
    response = requests.get(URL + '/data' + '/' + key)
    assert response.text == json.dumps(data[key])
    assert response.status_code == 200

def st_put_id(key, value):
    tmp_data = {'value':value}
    response = requests.put(URL + '/data' + '/' + key, data=tmp_data)
    if data[key] is None:
        tmp_data = {}
    else:
        data[key] = value
    assert response.text == json.dumps(tmp_data)
    assert response.status_code == 200

def st_delete_id(key):
    response = requests.delete(URL + '/data' + '/' + key)
    if data[key] is None:
        tmp_data = {}
    else:
        tmp_data = 'deleted'
        data.pop(key, None)
    assert response.text == json.dumps(tmp_data)
    assert response.status_code == 200


def test_post():
    st_post(key='1', value='2')
    st_post(key='1', value='3')
    st_post(key='2', value='3')

def test_get():
    response = requests.get(URL + '/data')
    assert response.text == json.dumps(data)
    assert response.status_code == 200

def test_get_id():
    st_get_id('1')
    st_get_id('2')
    st_get_id('3')

def test_get2():
    response = requests.get(URL + '/data')
    assert response.text == json.dumps(data)
    assert response.status_code == 200

def test_put_id():
    st_put_id('1', '3')
    st_put_id('2', '4')
    st_put_id('3', '5')

def test_get3():
    response = requests.get(URL + '/data')
    assert response.text == json.dumps(data)
    assert response.status_code == 200

def test_delete_id():
    st_delete_id('1')
    st_delete_id('2')
    st_delete_id('3')

def test_get4():
    response = requests.get(URL + '/data')
    assert response.text == json.dumps(data)
    assert response.status_code == 200